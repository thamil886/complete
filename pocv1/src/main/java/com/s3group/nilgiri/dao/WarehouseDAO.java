package com.s3group.nilgiri.dao;

import java.util.List;

import com.s3group.nilgiri.entity.Warehouse;

public interface WarehouseDAO {
	
	public List<Warehouse> findAllWarehouses();
	
	public List<Warehouse> saveWarehouse(Warehouse Warehouse);
	
	public List<Warehouse> updateWarehouse(Warehouse Warehouse);
	
	public void deleteWarehouse(Long warehouseId);
	
	public Warehouse findByWarehouseId(Long warehouseId);

}