package com.s3group.nilgiri.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.s3group.nilgiri.entity.Counter;

@Repository
@Transactional
public class CounterDAOImpl implements CounterDAO {
	
	public static final Logger logger = LoggerFactory.getLogger(CounterDAOImpl.class);
	
	@PersistenceContext	
	private EntityManager entityManager;	
	
	@Override
	public Counter findByCounterId(Long id) {
		return entityManager.find(Counter.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Counter> findAllCounters(){		
		String hql = "FROM Counter as ct ORDER BY ct.counterId";
		return (List<Counter>) entityManager.createQuery(hql).getResultList();		
	}
	
	@Override
	public List<Counter> saveCounter(Counter counter) {	
		List<Counter> counters = new ArrayList<Counter>();
		try {
			logger.info("Counter = "+counter.getCompany());
			entityManager.persist(counter);	
			logger.info("Persisted");
			counters = findAllCounters();
			logger.info("CountersSize = "+counters.size());
		}catch(Exception e) {e.printStackTrace();}
		
		return counters;
	}
	
	@Override
	public List<Counter> updateCounter(Counter counter) {
		List<Counter> counters = new ArrayList<Counter>();
		Counter ct = findByCounterId(counter.getCounterId());		
		if(ct == null) {
			logger.error("Unable to update. Counter with id {} not found.", counter.getCounterId());
		}else {			
			logger.info("Existing Desc = " +ct.getDescription()+" New Desc = "+counter.getDescription());
			entityManager.merge(counter);
			logger.info("Updated");
			counters = findAllCounters();
		}
		
		return counters;
	}
	
	@Override
	public void deleteCounter(Long id) {		
		entityManager.remove(findByCounterId(id));
		logger.info("Deleted");	
	
	}
}