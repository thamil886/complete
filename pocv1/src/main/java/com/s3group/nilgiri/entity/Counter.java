package com.s3group.nilgiri.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="COUNTER")
public class Counter implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COUNTER_ID")
	private Long counterId;
	
	@Column(name="COMPANY")
	private String company;
	
	@Column(name="DIVISION")
	private String division;
	
	@Column(name="WAREHOUSE")
	private String warehouse;
	
	@Column(name="RECORD_TYPE")
	private String recordType;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="PREFIX")
	private String prefix;
	
	@Column(name="PRE_LENGTH")
	private double preLength;
	
	@Column(name="START_NUMBER")
	private double startNumber;
	
	@Column(name="END_NUMBER")
	private double endNumber;
	
	@Column(name="CURRENT_NUMBER")
	private double currentNumber;
	
	@Column(name="CURRENT_NUMBER_LENGTH")
	private double currentNumberLength;
	
	@Column(name="INCREMENT_NUMBER")
	private double incrementNumber;
	
	@Column(name="CHECK_DIGIT_TYPE")
	private String checkDigitType;
	
	@Column(name="CHECK_DIGIT_LENGTH")
	private double checkDigitLength;
	

    @Column(name="RESET_FLAG")
    private String resetFlag;

    @Column(name="CHECK_DIGIT_FLAG")
    private String checkDigitFlag;

	@Column(name="SYSTEM_EXPANSION")
	private String systemExpansion;
	
	@Column(name="CUSTOMER_EXPANSION")
	private String customerExpansion;

	public Long getCounterId() {
		return counterId;
	}

	public void setCounterId(Long counterId) {
		this.counterId = counterId;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public double getPreLength() {
		return preLength;
	}

	public void setPreLength(double preLength) {
		this.preLength = preLength;
	}

	public double getStartNumber() {
		return startNumber;
	}

	public void setStartNumber(double startNumber) {
		this.startNumber = startNumber;
	}

	public double getEndNumber() {
		return endNumber;
	}

	public void setEndNumber(double endNumber) {
		this.endNumber = endNumber;
	}

	public double getCurrentNumber() {
		return currentNumber;
	}

	public void setCurrentNumber(double currentNumber) {
		this.currentNumber = currentNumber;
	}

	public double getCurrentNumberLength() {
		return currentNumberLength;
	}

	public void setCurrentNumberLength(double currentNumberLength) {
		this.currentNumberLength = currentNumberLength;
	}

	public double getIncrementNumber() {
		return incrementNumber;
	}

	public void setIncrementNumber(double incrementNumber) {
		this.incrementNumber = incrementNumber;
	}

	public String getCheckDigitType() {
		return checkDigitType;
	}

	public void setCheckDigitType(String checkDigitType) {
		this.checkDigitType = checkDigitType;
	}

	public double getCheckDigitLength() {
		return checkDigitLength;
	}

	public void setCheckDigitLength(double checkDigitLength) {
		this.checkDigitLength = checkDigitLength;
	}

	public String getResetFlag() {
		return resetFlag;
	}

	public void setResetFlag(String resetFlag) {
		this.resetFlag = resetFlag;
	}

	public String getCheckDigitFlag() {
		return checkDigitFlag;
	}

	public void setCheckDigitFlag(String checkDigitFlag) {
		this.checkDigitFlag = checkDigitFlag;
	}

	public String getSystemExpansion() {
		return systemExpansion;
	}

	public void setSystemExpansion(String systemExpansion) {
		this.systemExpansion = systemExpansion;
	}

	public String getCustomerExpansion() {
		return customerExpansion;
	}

	public void setCustomerExpansion(String customerExpansion) {
		this.customerExpansion = customerExpansion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

		
}
