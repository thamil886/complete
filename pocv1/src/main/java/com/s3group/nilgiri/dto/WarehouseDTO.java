package com.s3group.nilgiri.dto;

public class WarehouseDTO {
	
	//private Long companyId;
	//private Long divisionId;
	
	private Long warehouseId;
	private String whsewarehouse;
    private String whsedesc;
	private String whseaddress1;
	private String whseaddress2;
	private String whsecity;
	private String whsestate;
	private int whsezip;
	private String whsecountry;
	private String whsecontactname;
	private int whsecontactnumber;
	private String whsealtaddress1;
	private String whsealtaddress2;
	private String whsealtcity;
	private String whsealtstate;
	private int whsealtzip;
	private String whsealtcountry;


public WarehouseDTO() {}

/*public Long getCompanyId() {
	return companyId;
}
public void setCompanyId(Long companyId) {
	this.companyId = companyId;
}
public Long getDivisionId() {
	return divisionId;
}
public void setDivisionId(Long divisionId) {
	this.divisionId = divisionId;
}*/

public Long getWarehouseId() {
	return warehouseId;
}
public void setWarehouseId(Long warehouseId) {
	this.warehouseId = warehouseId;
}

public String getWarehouse() {
	return whsewarehouse;
}
public void setWarehouse(String warehouse) {
	this.whsewarehouse = warehouse;
}
public String getDesc() {
	return whsedesc;
}
public void setDesc(String description) {
	this.whsedesc = description;
}
public String getAddress1() {
	return whseaddress1;
}
public void setAddress1(String address1) {
	this.whseaddress1 = address1;
}
public String getAddress2() {
	return whseaddress2;
}
public void setAddress2(String address2) {
	this.whseaddress2 = address2;
}
public String getCity() {
	return whsecity;
}
public void setCity(String city) {
	this.whsecity = city;
}
public String getState() {
	return whsestate;
}
public void setState(String state) {
	this.whsestate = state;
}
public int getZip() {
	return whsezip;
}
public void setZip(int zip) {
	this.whsezip = zip;
}
public String getCountry() {
	return whsecountry;
}
public void setCountry(String country) {
	this.whsecountry = country;
}
public String getContactName() {
	return whsecontactname;
}
public void setContactName(String contactname) {
	this.whsecontactname = contactname;
}
public int getContactNumber() {
	return whsecontactnumber;
}
public void setContactNumber(int contactnumber) {
	this.whsecontactnumber = contactnumber;
}
public String getAltAddress1() {
	return whsealtaddress1;
}
public void setAltAddress1(String altaddress1) {
	this.whsealtaddress1 = altaddress1;
}

public String getAltAddress2() {
	return whsealtaddress2;
}
public void setAltAddress2(String altaddress2) {
	this.whsealtaddress2 = altaddress2;
}
public String getAltCity() {
	return whsealtcity;
}
public void setAltCity(String altcity) {
	this.whsealtcity = altcity;
}

public String getAltState() {
	return whsealtstate;
}
public void setAltState(String altstate) {
	this.whsealtstate = altstate;
}
public int getAltZip() {
	return whsealtzip;
}
public void setAltZip(int altzip) {
	this.whsealtzip = altzip;
}
public String getAltCountry() {
	return whsealtcountry;
}
public void setAltCountry(String altcountry) {
	this.whsealtcountry = altcountry;
}


}

