package com.s3group.nilgiri.dto;

import java.util.ArrayList;
import java.util.List;

public class LocationDTO {
	
	private Long id;
	private String company;
	private String division;
	private String warehouse;
	private String locationType; 
	private String area;
	private String zone;
	private String aisle;
	private String bay;
	private String level;
	private String position;
	private int sequenceNumber;
	private String locnBarcode;
	private String locnSizeType;
	private String locnProductType;
	private String putawayZone;
	private char locnLocked;
	private String invenLockCode;
	private String statusFlag;
		
	private List <LocationAtr1DTO> locations1 = new ArrayList<LocationAtr1DTO>();
	private List <LocationAtr2DTO> locations2 = new ArrayList<LocationAtr2DTO>();
	
	public LocationDTO() {}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCompany() {
		return company;
	}

	public String getDivision() {
		return division;
	}
	
	public void setCompany(String company) {
		this.company = company;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getAisle() {
		return aisle;
	}
	public void setAisle(String aisle) {
		this.aisle = aisle;
	}
	public String getBay() {
		return bay;
	}
	public void setBay(String bay) {
		this.bay = bay;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getLocnBarcode() {
		return locnBarcode;
	}

	public void setLocnBarcode(String locnBarcode) {
		this.locnBarcode = locnBarcode;
	}

	public String getLocnSizeType() {
		return locnSizeType;
	}

	public void setLocnSizeType(String locnSizeType) {
		this.locnSizeType = locnSizeType;
	}

	public String getLocnProductType() {
		return locnProductType;
	}

	public void setLocnProductType(String locnProductType) {
		this.locnProductType = locnProductType;
	}

	public String getPutawayZone() {
		return putawayZone;
	}

	public void setPutawayZone(String putawayZone) {
		this.putawayZone = putawayZone;
	}

	public char getLocnLocked() {
		return locnLocked;
	}

	public void setLocnLocked(char locnLocked) {
		this.locnLocked = locnLocked;
	}

	public String getInvenLockCode() {
		return invenLockCode;
	}

	public void setInvenLockCode(String invenLockCode) {
		this.invenLockCode = invenLockCode;
	}

	public String getStatusFlag() {
		return statusFlag;
	}

	public void setStatusFlag(String statusFlag) {
		this.statusFlag = statusFlag;
	}

	public List<LocationAtr1DTO> getLocations1() {
		return locations1;
	}

	public void setLocations1(List<LocationAtr1DTO> locations1) {
		this.locations1 = locations1;
	}

	public List<LocationAtr2DTO> getLocations2() {
		return locations2;
	}

	public void setLocations2(List<LocationAtr2DTO> locations2) {
		this.locations2 = locations2;
	}

}
