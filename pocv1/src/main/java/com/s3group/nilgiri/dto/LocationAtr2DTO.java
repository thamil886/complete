package com.s3group.nilgiri.dto;

public class LocationAtr2DTO {
	
	private Long id;
	//private String locationId;
	private String company;
	private String division;
	private String warehouse;
	private String locationType;
	private String locnBarcode;
	private String itemBarcode;
	
	public LocationAtr2DTO() {};
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	/*public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}*/

	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	public String getLocationType() {
		return locationType;
	}
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	public String getLocnBarcode() {
		return locnBarcode;
	}
	public void setLocnBarcode(String locnBarcode) {
		this.locnBarcode = locnBarcode;
	}
	public String getItemBarcode() {
		return itemBarcode;
	}
	public void setItemBarcode(String itemBarcode) {
		this.itemBarcode = itemBarcode;
	}
	
}
