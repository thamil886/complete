package com.s3group.nilgiri.service;

import java.util.List;

import com.s3group.nilgiri.dto.WarehouseDTO;
import com.s3group.nilgiri.entity.Warehouse;

public interface WarehouseService {
	
public List<WarehouseDTO> findAllWarehouses();
	
	public List<WarehouseDTO> saveWarehouse(WarehouseDTO warehouseDTO);
	
	public List<WarehouseDTO> updateWarehouse(WarehouseDTO warehouseDTO);
	
	public void deleteWarehouse(Long warehouseId);
	
	public Warehouse findByWarehouseId(Long warehouseId);

}
