package com.s3group.nilgiri.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.s3group.nilgiri.dao.LocationDAO;
import com.s3group.nilgiri.dto.LocationAtr1DTO;
import com.s3group.nilgiri.dto.LocationAtr2DTO;
import com.s3group.nilgiri.dto.LocationDTO;
import com.s3group.nilgiri.entity.Location;
import com.s3group.nilgiri.entity.LocationAtr1;
import com.s3group.nilgiri.entity.LocationAtr2;

@Service
public class LocationServiceImpl implements LocationService {
	
public static final Logger logger = LoggerFactory.getLogger(LocationServiceImpl.class);
	
	/*@Autowired
	private LocationRepository locationRepository;*/
	
	@Autowired
	private LocationDAO locationDAO;
	
	
	public List<LocationDTO> findAllLocations(){				
		List <Location> locationLists = locationDAO.findAllLocations();
		//List <Location> locationLists = locationRepository.findAll();		
		List<LocationDTO> locationDTOs = new ArrayList<LocationDTO>();
		for(Location location : locationLists) {
			LocationDTO locationDTO = buildLocationDTO(location);			
			locationDTOs.add(locationDTO);
		}	
		return locationDTOs;
	}	
	
	public List<LocationDTO> saveLocation(LocationDTO locationDTO) {			
		Location location = buildLocation(locationDTO);		
		List <Location> locationLists = locationDAO.saveLocation(location);	
		List<LocationDTO> locationDTOs = new ArrayList<LocationDTO>();
		for(Location lc : locationLists) {
			LocationDTO lcDTO = buildLocationDTO(lc);	
			locationDTOs.add(lcDTO);
		}
		//locationRepository.save(location);
		return locationDTOs;
	}
	
	public List<LocationDTO> updateLocation(LocationDTO locationDTO) {
		Location location = buildLocation(locationDTO);
		List <Location> locationLists = locationDAO.updateLocation(location);
		List<LocationDTO> locationDTOs = new ArrayList<LocationDTO>();
		for(Location lc : locationLists) {
			LocationDTO lcDTO = buildLocationDTO(lc);	
			locationDTOs.add(lcDTO);
		}
		//locationRepository.save(location);
		return locationDTOs;
	}

	public void deleteLocation(Long id) {
		locationDAO.deleteLocation(id);
		//locationRepository.deleteById(id);	
	}
	
	public Location findByLocationId(Long id) {
		Location location = locationDAO.findByLocationId(id);
		//Location location = locationRepository.findByLocationId(id);	
		return location;
	}
	
	private Location buildLocation(LocationDTO locationDTO) {		
		Location location = new Location();		
		location.setLocationId(locationDTO.getId());	
		location.setCompany(locationDTO.getCompany());
		location.setDivision(locationDTO.getDivision());
		location.setWarehouse(locationDTO.getWarehouse());
		location.setLocationType(locationDTO.getLocationType());
		location.setArea(locationDTO.getArea());
		location.setAisle(locationDTO.getAisle());
		location.setBay(locationDTO.getBay());
		location.setZone(locationDTO.getZone());
		location.setInvenLockCode(locationDTO.getInvenLockCode());
		location.setLevel(locationDTO.getLevel());
		location.setPosition(locationDTO.getPosition());
		location.setSequenceNumber(locationDTO.getSequenceNumber());
		location.setLocnBarcode(locationDTO.getLocnBarcode());
		location.setLocnProductType(locationDTO.getLocnProductType());
		location.setLocnSizeType(locationDTO.getLocnSizeType());
		location.setPutawayZone(locationDTO.getPutawayZone());
		location.setStatusFlag(locationDTO.getStatusFlag());
		location.setLocnLocked(locationDTO.getLocnLocked());
		
		location.setLocations1(new ArrayList<>());
		for(LocationAtr1DTO locn1DTO : locationDTO.getLocations1()){
			LocationAtr1 locationAtr1 = new LocationAtr1();
			BeanUtils.copyProperties(locn1DTO, locationAtr1);
			locationAtr1.setLocation(location);			
			location.getLocations1().add(locationAtr1);			
		}
		
		location.setLocations2(new ArrayList<>());
		for(LocationAtr2DTO locn2DTO : locationDTO.getLocations2()){
			LocationAtr2 locationAtr2 = new LocationAtr2();
			BeanUtils.copyProperties(locn2DTO, locationAtr2);
			locationAtr2.setLocation(location);
			location.getLocations2().add(locationAtr2);			
		}
		
		return location;
	}
	
	
	private LocationDTO buildLocationDTO(Location location) {					
		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setId(location.getLocationId());	
		locationDTO.setCompany(location.getCompany());
		locationDTO.setDivision(location.getDivision());
		locationDTO.setWarehouse(location.getWarehouse());
		locationDTO.setLocationType(location.getLocationType());
		locationDTO.setArea(location.getArea());
		locationDTO.setAisle(location.getAisle());
		locationDTO.setBay(location.getBay());
		locationDTO.setZone(location.getZone());
		locationDTO.setInvenLockCode(location.getInvenLockCode());
		locationDTO.setLevel(location.getLevel());
		locationDTO.setPosition(location.getPosition());
		locationDTO.setSequenceNumber(location.getSequenceNumber());
		locationDTO.setLocnBarcode(location.getLocnBarcode());
		locationDTO.setLocnProductType(location.getLocnProductType());
		locationDTO.setLocnSizeType(location.getLocnSizeType());
		locationDTO.setPutawayZone(location.getPutawayZone());
		locationDTO.setStatusFlag(location.getStatusFlag());
		locationDTO.setLocnLocked(location.getLocnLocked());
		
		locationDTO.setLocations1(new ArrayList<>());
		for(LocationAtr1 locn1 : location.getLocations1()){
			LocationAtr1DTO locationAtr1DTO = new LocationAtr1DTO();
			BeanUtils.copyProperties(locn1, locationAtr1DTO);
			locationDTO.getLocations1().add(locationAtr1DTO);			
		}
		
		locationDTO.setLocations2(new ArrayList<>());
		for(LocationAtr2 locn2 : location.getLocations2()){
			LocationAtr2DTO locationAtr2DTO = new LocationAtr2DTO();
			BeanUtils.copyProperties(locn2, locationAtr2DTO);
			locationDTO.getLocations2().add(locationAtr2DTO);			
		}
		
		return locationDTO;
	}

}
